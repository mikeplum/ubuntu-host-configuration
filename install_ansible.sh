#!/bin/bash

# Adding Ansible repository to ppa list if does not exist
if ! grep -q "anisble/ansible" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
		echo "Adding Ansible PPA"
  	sudo apt-add-repository ppa:ansible/ansible -y
fi

# Install Ansible if does not exists
if ! hash ansible >/dev/null 2>&1; then
    echo "Installing Ansible..."
    sudo apt-get update
    sudo apt-get install git ansible -y
else
    echo "Ansible already installed"
fi
